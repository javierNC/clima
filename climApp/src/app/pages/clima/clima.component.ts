import { Component, OnInit } from '@angular/core';
import { ClimaService } from '../../shared/services/clima.service';


@Component({
  selector: 'app-clima',
  templateUrl: './clima.component.html',
  styleUrls: ['./clima.component.css']
})
export class ClimaComponent implements OnInit {

  climas: any;
  predicciones: any;

  constructor( private climaService: ClimaService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.climaService.getClima().subscribe((res: any)=>{
      this.climas = res;
      console.log(this.climas);
    });
    this.climaService.getPrediccion().subscribe((r:any)=>{
      this.predicciones = r;
      console.log(this.predicciones);
    });
  };

}
