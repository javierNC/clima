import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClimaService {
  ciudades:{
    c1: 'Bogota',
    c2: 'Madrid',
    c3: 'Lima'
  };

  horarios:{
    dia: '06:00',
    tarde: '12:00',
    noche: '18:00'
  };

  constructor(private http: HttpClient) { }

  buscarCiudades(ciudades){
    return this.http.post('http://api.openweathermap.org/data/2.5/weather?q=',{ciudades},'&appid=2e04a5779a48a5a5f96149e8c0e103af');
  };

  getClima(){
    return this.http.get('http://api.openweathermap.org/data/2.5/forecast?q=Mexico&appid=2e04a5779a48a5a5f96149e8c0e103af');
  };

  getPrediccion(){
    return this.http.get(environment.forecast);
  };

}
